# Stechuhr #

Work time logging with [Rainmeter Skin](https://www.rainmeter.net/).

License: MIT

## Highlights ##

- Tracks how much time you worked each: Day, Week, Month and Year.
- If set, it will visually remark you, if you worked to much.
- Over and under hours are taken into account for the next day, so you can allways see your missing or over done time.
- Track time spent on tasks in 3 phases: Preparation, Doing, Controlling
- Get everything in a easy-to-read-log file, that can be imported into an excel sheet for further analysation.
- Track your leave days.

## Worktime settings ##
A whole cycle marks always the typical work time
- `day` : Number: hours per day
- `week` : Number: hours per week
- `holidaysPerMonth`: Number: Holidays earnt per month
- `ignoreBreak` : Number: Minutes of inactivity, that shall not be logged as a break.

Missing month and year values will be calculated. A whole daily work time circle is always the time entered in day. If less time is necessary, the clock ends earlier, if more time is necessary, the clock starts earlier. If the sum of all work days exceeds the week time, the week time will be taken into account. This means for `day = 8` and `week = 38` it will assume, you work `6` hours on friday, if you worked `8` hours every day from monday to thursday.

- `weekStartDay` : Declares the first day of the new week. During the week, over hours will be calculated from day to day. This means, if you finish the last day wihtout the clock showing an over hour, you did not exceed the weekly worktime. If you worked too much hours a week, the next week will be reset and the over hours will be added to you over-hours-account.

## Source tree ##

- `@Resources/`: Contains all resources like settings, scripts and stores log files.
- `@Resources/scripts/`: Contains the script that realises the main functionality.
- `@Resources/temps/`: Will contain temporary files for measuring your daily work time.
- `@Resources/settings.inc`: There your work time has to be set. Contains the variables devlared above.
- `@Resources/styles.inc`: Style definition. Color adaptation und scaling is defined here.
- `@Resources/worktime.inc`: Contains sumarized information of your daily work time for the calculation of the remaining worktime.
- `@Resources/worktime.csv`: Contains more detailed logs of your worktime in a CSV-format for e.g. Excel.
- `uhr.ini`: actual Skin, that is loaded by Rainmeter.

## Contact ##

<dennis@vdwals.de>
