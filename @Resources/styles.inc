[TitleStyle]
FontSize=14
FontFace=#FontFace#
FontColor=#TextColor1#
AntiAlias=1

[StatusStyle]
FontSize=8
FontFace=#FontFace#
FontColor=#TextColor2#
AntiAlias=1

[TitleProcessStyle]
FontSize=12
FontFace=#FontFace#
FontColor=#TextColor1#
AntiAlias=1

[TextStyle]
FontSize=10
FontFace=#FontFace#
FontColor=#TextColor2#
AntiAlias=1

[HistogramStyle]
PrimaryColor=#PrimaryColor#
SecondaryColor=#SecondaryColor#
BothColor=#BothColor#
AntiAlias=1
W=210
H=60

[RotConst]
StartAngle=#startangle#
RotationAngle=#rotationangle#
AntiAlias=1
W=#size#
H=#size#
Z=1

[HoursWorkedConst]
LineStart=117
LineLength=130
LineColor=#hoursworkedcolor#
Solid=1

[WeekWorkedConst]
LineStart=132
LineLength=140
LineColor=#weekworkedcolor#
Solid=1

[StringConst]
StringAlign=CenterCenter
FontSize=14
FontColor=#TextColor1#
FontFace=#FontFace#
StringEffect=Shadow
AntiAlias=1

[BreakConst]
FontSize=12
FontColor=#TextColor2#
StringAlign=LeftCenter

[BreakDelConst]
FontColor=172,28,28,#Alpha2#

[StyleProcessText]
X=12
Y=13r
H=17
W=((#SidebarWidth#-24-#Col2Width#)*#Col1WidthMod#)
ClipString=1
StringStyle=NORMAL
FontColor=#Color1#
FontSize=#Size4#
FontFace=#Font#
AntiAlias=1
ToolTipWidth=#SidebarWidth#
DynamicVariables=1

[StyleProcessNumber]
X=#Col2Width#R
Y=r
W=#Col2Width#
H=17
StringStyle=BOLD
StringAlign=RIGHT
FontColor=#Color1#
FontSize=#Size3#
FontFace=#Font#
AntiAlias=1
DynamicVariables=1

[StyleProcessBar]
X=5r
Y=7r
W=(#SidebarWidth#-24-#Col2Width#-((#SidebarWidth#-24-#Col2Width#)*#Col1WidthMod#)-5)
H=4
BarColor=#Color1#
SolidColor=#Color3#
SolidColor2=#Color4#
BarOrientation=HORIZONTAL
