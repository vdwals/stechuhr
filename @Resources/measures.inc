[mSec]
Measure=Time
Format=%#S

[mHours]
Measure=Time
Format=%#H

[mDate]
Measure=Time
Format=%A, %B %#d, %Y %H:%M:%S

[ScriptMeasure]
Measure=Script
ScriptFile=#@#/scripts/log.lua