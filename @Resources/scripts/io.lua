ioHelp = {}

-- Returns context of the given file.
function ioHelp.ReadTempFile(FilePath)
	if (DEBUG) then
			print('ReadTempFile: '..FilePath..'(')
		end
	-- HANDLE RELATIVE PATH OPTIONS.

	if not ioHelp.temp_file_exists(FilePath) then
		if (DEBUG) then
			print('File does not exist: ' .. FilePath..')')
		end
		return {}
	end

	FilePath = ioHelp.GetTempFilePath(FilePath)

	-- OPEN FILE.
	local file = io.open(FilePath, 'r')
	lines = {}
	for line in file:lines() do
		if (DEBUG) then
			print(#lines..': '..line)
		end
		lines[#lines + 1] = line
	end
	if (DEBUG) then
			print(#lines .. ' lines read from ' .. FilePath..')')
		end
	return lines
end

-- Write into the parametered file the given content. Modus can be "w" for replace and "a" for append
function ioHelp.WriteTempFile(FilePath, Contents, Modus)
	if (DEBUG) then
			print('WriteTempFile(')
		end
	-- HANDLE RELATIVE PATH OPTIONS.
	FilePath = ioHelp.GetTempFilePath(FilePath)
	-- OPEN FILE.
	local File = io.open(FilePath, Modus)
	-- HANDLE ERROR OPENING FILE.
	if not File and DEBUG then
		print('WriteTempFile: unable to open file at ' .. FilePath..')')
		return
	end

	-- WRITE CONTENTS AND CLOSE FILE
	File:write(Contents)
	File:close()
	if (DEBUG) then
			print('Write to File: ' .. FilePath .. '; ' .. Modus..')')
		end
	return true
end

-- Checks if file exists
function ioHelp.file_exists(file)
	if (DEBUG) then
			print('Check if file exists: ' .. file..'(')
		end
	local f = io.open(file, "rb")
	if f then
		f:close()
		if (DEBUG) then
			print('File exists)')
		end
	elseif DEBUG then
		print(')')
	end
	return f ~= nil
end

--The returned table is in the format Table[Section][Parameter] = Value
function ioHelp.ReadParameterFile(file)
	if (DEBUG) then
			print('Reading parameter file: ' .. file..'(')
		end
	local inputfile = ioHelp.GetTempFilePath(file)

	local tbl = {}
	if ioHelp.file_exists(inputfile) then
		local file = assert(io.open(inputfile, 'r'), 'Unable to open ' .. inputfile..')')

		local num = 0
		for line in file:lines() do
			num = num + 1
			if not line:match('^%s-;') then
				local key, command = line:match('^([^=]+)=(.+)')
				if key and command then
					if (DEBUG) then
			print(num..': '..key:lower():match('^%s*(%S*)%s*$').."="..command:match('^%s*(.-)%s*$'))
		end
					tbl[key:lower():match('^%s*(%S*)%s*$')] = command:match('^%s*(.-)%s*$')
				elseif (#line > 0 and not key or command) and DEBUG then
					print(num .. ': Invalid property or value.)')
				end
			end
		end
	end
	if (DEBUG) then
			print(')')
		end
	return tbl
end

--The table must be in the format Table[Section][Parameter] = Value
function ioHelp.WriteParameterFile(file, parameter)
	if (DEBUG) then
			print('Writing parameter file: ' .. file..'(')
		end
	local filename = ioHelp.GetTempFilePath(file)
	assert(type(parameter) == 'table', ('WriteParameterFile must receive a table. Received %s instead.)'):format(type(parameter)))
	local file = assert(io.open(filename, 'w'), 'Unable to open ' .. filename..')')
	local lines = {}
	for key, value in pairs(parameter) do
		local formatted = ('%s=%s'):format(key, value);
		if (DEBUG) then
			print(formatted)
		end
		table.insert(lines, formatted)
	end
	file:write(table.concat(lines, '\r\n'))
	file:close()
	if (DEBUG) then
			print(')')
		end
end

--The returned table is in the format Table[Section][Parameter] = Value
function ioHelp.ReadLogHistory()
	if (DEBUG) then
			print('Reading ini file(')
		end
	ini = {}
	ini['settings'] = {}
	ini['settings']['lastdate'] = ''
	ini['settings']['week'] = 0

	local inputfile = ioHelp.GetFilePath(timerLogFile)
	if (not ioHelp.file_exists(inputfile)) then
		return
	end
	local file = assert(io.open(inputfile, 'r'), 'Unable to open ' .. inputfile..')')

	local section = {}
	local num = 0
	for line in file:lines() do
		num = num + 1
		if not line:match('^%s-;') then
			local key, command = line:match('^([^=]+)=(.+)')
			if line:match('^%s-%[.+') then
				section = line:match('^%s-%[([^%]]+)'):lower()
				if not ini[section] then ini[section] = {} end
			elseif key and command and section then
				ini[section][key:lower():match('^%s*(%S*)%s*$')] = command:match('^%s*(.-)%s*$')
			elseif (#line > 0 and section and not key or command) and DEBUG then
				print(num .. ': Invalid property or value.')
			end
		end
	end
	if DEBUG then
		if not section then
			print('No sections found in logfile.ini)')
		else
			print(')')
		end
	end
end

--The table must be in the format Table[Section][Parameter] = Value
function ioHelp.WriteLogHistory()
	if (DEBUG) then
			print('Writing history log file(')
		end
	local filename = ioHelp.GetFilePath(timerLogFile)
	local inputtable = ini
	assert(type(inputtable) == 'table', ('WriteLogHistory must receive a table. Received %s instead.)'):format(type(inputtable)))
	local file = assert(io.open(filename, 'w'), 'Unable to open ' .. filename..')')
	local lines = {}
	for section, contents in pairs(inputtable) do
		table.insert(lines, ('\[%s\]'):format(section))
		for key, value in pairs(contents) do
			table.insert(lines, ('%s=%s'):format(key, value))
		end
		table.insert(lines, '')
	end
	file:write(table.concat(lines, '\r\n'))
	file:close()
	if (DEBUG) then
			print(')')
		end
end

-- Checks if the given file exists in the resources path
function ioHelp.temp_file_exists(file)
	if file == nil then
		return false
	end
	if (DEBUG) then
			print('temp_file_exists: '..file)
		end
	return ioHelp.file_exists(ioHelp.GetTempFilePath(file))
end

-- Returns the file path string for temporary files.
function ioHelp.GetTempFilePath(filePath)
	if (DEBUG) then
			print('GetTempFilePath: '..filePath)
		end
	return ioHelp.GetFilePath('temps\\'..filePath)
end

function ioHelp.GetFilePath(filename)
	if (DEBUG) then
			print('GetFilePath: '..filename)
		end
	return SKIN:GetVariable('@')..filename
end
