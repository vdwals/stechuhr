-- Global variables --
timerLogFile='worktime.inc'
dailyLogFileEnding='_parameter'
dailyTickFileEnding='_tick'
initialized=false
breakActive=0
breakTimerShown=false

DEBUG=false

-- Init functions ---------------------
function Initialize()
	if (DEBUG) then
		print('Initialize (')
	end
	-- Include other Scripts
	dofile(SKIN:GetVariable('@')..'scripts\\io.lua')
	dofile(SKIN:GetVariable('@')..'scripts\\helper.lua')

	InitTimeMeasuring()
	InitGUIParams()

	initialized=true
	if (DEBUG) then
		print(')')
	end
end

function InitTimeMeasuring()
	if (DEBUG) then
		print('InitTimeMeasuring (')
	end
	SetToday()

	-- Load settings file.
	ioHelp.ReadLogHistory()


	if (DEBUG) then
		print('Initialize Script:'..today)
	end

	-- Check if a previous day has to be finished before starting the new day.
	if ini['settings']['lastdate'] and ini['settings']['lastdate'] ~= '' then

		if (DEBUG) then
			print('Lastdate vorhanden: '..ini['settings']['lastdate'])
		end
		if ini['settings']['lastdate'] == today then
			if (DEBUG) then
				print(ini['settings']['lastdate']..' == '..today)
			end
		else
			if (DEBUG) then
				print(ini['settings']['lastdate']..' ~= '..today)
			end
			HandleWorkDayEnd(ini['settings']['lastdate'])
		end
	elseif DEBUG then
		print('Lastdate nicht vorhanden')
	end

	--Load or Initialize the daily time measure table.
	InitDailyTbl()

	-- Check if tick-file already exists and determine the break time.
	updateBreakTime()

	-- Create or update dailyLogFile with current start time.
	writeTodaysParameterFile()

	-- Set today to the last logged day.
	ini['settings']['lastdate'] = today

	-- Save settings.
	ioHelp.WriteLogHistory()

	if (DEBUG) then
		print(')')
	end
end

function CalcTimeToWorkToday()
	if (DEBUG) then
		print('CalcTimeToWorkToday(')
	end
	if not dailyTbl['secondstowork'] then
		local typicalSecondsToWork = SKIN:GetVariable('day', '8') * 60 * 60

		local workWeekDay = help.GetWorkdayOfWeek(today, SKIN:GetVariable('weekStartDay'))
		-- No caluclation necessarym if this is the start of the week.
		if (workWeekDay == 0) then
			dailyTbl['secondstowork'] = typicalSecondsToWork

			if (DEBUG) then
				print(dailyTbl['secondstowork']..')')
			end
			ini['settings']['week'] = 0
			return
		end

		-- Calc the amount of time that should be worked each day
		-- Number of seconds per week
		local weeklySecondsToWork = GetSecondsToWorkPerWeek()
		-- Number of seconds to be worked this week until end of day

		if (DEBUG) then
			print((workWeekDay + 1))
			print(typicalSecondsToWork)
		end
		local workedUntilEOD = (workWeekDay + 1) * typicalSecondsToWork

		if (DEBUG) then
			print(weeklySecondsToWork)
			print(workedUntilEOD)
			print(ini['settings']['week'])
		end

		if workedUntilEOD > weeklySecondsToWork then
			-- If the sum of all days is greater than the work time per week, take only the work time per week into account.
			dailyTbl['secondstowork'] = weeklySecondsToWork - ini['settings']['week']
		else
			-- else, take the typical daywork into account.
			dailyTbl['secondstowork'] = workedUntilEOD - ini['settings']['week']
		end

		-- Adjust circle size depending of full work time, only if the time difference is greater than the update intervall to prevent useless calculation.
		if math.abs(dailyTbl['secondstowork'] - typicalSecondsToWork) > GetUpdateIntervallInSeconds() then

			if (DEBUG) then
				print('Change circle end')
			end
			local circleEnd = SKIN:GetVariable('rotationangle', '6,283185307179586476925286766559')

			if (DEBUG) then
				print('Setting: '..circleEnd)
			end
			local percent = dailyTbl['secondstowork'] / typicalSecondsToWork

			if (DEBUG) then
				print('Percent: '..percent)
			end
			circleEnd = circleEnd * percent

			if (DEBUG) then
				print('New Value: '..circleEnd)
			end
			SKIN:Bang('[!SetVariable rotationangle "'..circleEnd..'"]')
		end
	end

	if (DEBUG) then
		print(dailyTbl['secondstowork']..')')
	end
end

function InitGUIParams()
	if (DEBUG) then
		print('InitGUIParams(')
	end
	SetWorkTimeDisplay()

	if (DEBUG) then
		print(')')
	end
end

function SetToday()
	today=string.gsub(os.date('%x'),'/','-')

	if (DEBUG) then
		print('SetToday: '..today)
	end
end

function InitDailyTbl()
	if (DEBUG) then
		print('InitDailyTbl (')
	end
	dailyTbl = ioHelp.ReadParameterFile(GetDailyLogFileName(today))

	-- If starttime is not available, a new day starts now.
	if not dailyTbl['starttime'] then
		if (DEBUG) then
			print('Daily Parameter created')
		end
		dailyTbl['starttime'] = GetNow()
	end
	if not dailyTbl['break'] then
		dailyTbl['break'] = 0
	end

	CalcTimeToWorkToday()


	if (DEBUG) then
		print(')')
	end
end

-- Called Functions ----------------------
function delBreak(breakCount)
	if (DEBUG) then
		print('delBreak:()')
	end
	local starts = help.split(dailyTbl['breakstarts'],",")
	local breaksCount = help.count(starts)

	if (DEBUG) then
		print(breakCount..' - '..breaksCount..' : '..dailyTbl['break'])
	end
	if breaksCount >= breakCount then
		local ends = help.split(dailyTbl['breakends'],",")
		table.remove(starts, breakCount)
		table.remove(ends, breakCount)

		-- Clear existing strings
		dailyTbl['breakstarts'] = nil
		dailyTbl['breakends'] = nil
		dailyTbl['break'] = 0

		breaksCount = breaksCount - 1
		-- If still breaks exist, re-add the breaks
		for nameCount = 1, breaksCount do
			LogBreakStart(starts[nameCount])
			LogBreakEnd(ends[nameCount])

			dailyTbl['break'] = dailyTbl['break'] + tonumber(ends[nameCount] - starts[nameCount])
		end
		plotBreaks()
		writeTodaysParameterFile()
		SetWorkTimeDisplay()

		if (DEBUG) then
			print(')')
		end
	end
end

-- Common functions ----------------------
function writeTodaysParameterFile()
	ioHelp.WriteParameterFile(GetDailyLogFileName(today), dailyTbl)
end

function getLastTick()
	local loggedTicks = help.split(help.getFirstLine(GetDailyTickFileName(today)))
	return loggedTicks
end

function updateBreakTime()
	if (DEBUG) then
print('updateBreakTime(')
end

	-- If lastTick is available, the system stopped time measureing.
	local lastTick = help.getFirstLine(GetDailyTickFileName(today))
	if lastTick == 0 then
		plotBreaks()
		if (DEBUG) then
print('Keine Pause erkannt.)')
end
		return
	end

	if (DEBUG) then
print('Tick-Log: ' .. lastTick)
end
	local loggedTicks = help.split(lastTick)

	-- Calculate the last active tick by subtracting the last logged idle time.
	lastTick = loggedTicks[1] - loggedTicks[2]

	-- Calculate and update the break time.
	local now = GetNow()
	local ignoreSeconds = GetBreakIgnoreInSeconds()
	local breakTime = now - lastTick
	if (DEBUG) then
		print('Unterbrechung erkannt.')
	end
	if breakTime > ignoreSeconds then
		if (DEBUG) then
			print('Pause erkannt: '..now.." - "..lastTick.." = "..breakTime)
		end

		-- Determine if a break start was recognized before and set the breakActive trigger, so the break will be closed by the tick() function. This can occur, if the skin is refreshed after a break was recognized in between the update intervall, so the skin does not remember it already knew it there was a break.
		if tonumber(loggedTicks[2]) >= tonumber(ignoreSeconds) then
			if (DEBUG) then
				print('Pausenende wird von Tick behandelt.')
			end
			breakActive = breakTime
			tick()
		else
			-- If break was not recognizable before, log it now. This is always the case if the Skin was shut down without active break.
			if (DEBUG) then
print('Pausenende wird direkt behandelt.')
			print('Alte Pausenzeit: '..dailyTbl['break'])
end
			dailyTbl['break'] = dailyTbl['break'] + breakTime
			if (DEBUG) then
print('Neue Pausenzeit: '..dailyTbl['break'])
end
			breakActive = 0
			LogBreakStart(lastTick)
			LogBreakEnd(now)
		end
	end
	plotBreaks()
	if (DEBUG) then
print(')')
end
end

function plotBreaks()
	local starts = help.split(dailyTbl['breakstarts'],",")
	local ends = help.split(dailyTbl['breakends'],",")

	local breaksCount = math.min(help.count(starts),6)
	if (DEBUG) then
print('plotBreaks: '..breaksCount)
end

	for nameCount = 1, breaksCount do
		SetBreakLogDisplay(nameCount, starts[nameCount], ends[nameCount])
	end
	for nameCount = breaksCount + 1, 6 do
		ResetBreakLogDisplay(nameCount)
	end
	
	local result = GetTimeFromSeconds(dailyTbl['break'])
	SKIN:Bang('!SetOption', 'BreakLogTotal', 'Text', result)
end

function LogBreakStart(seconds)
	LogBreak('breakstarts', seconds)
end

function LogBreakEnd(seconds)
	-- Do not log a break end unless there is a matching number of break starts
	local startSize = 0
	local endSize = 0
	if dailyTbl['breakstarts'] then
	 	startSize = help.count(help.split(dailyTbl['breakstarts'],","))
	end
	if dailyTbl['breakends'] then
		endSize = help.count(help.split(dailyTbl['breakends'],","))
	end
	if (startSize - 1) == endSize then
		SetWorkTimeDisplay()
		LogBreak('breakends', seconds)
		plotBreaks();
	elseif DEBUG then
		print('Breakend without start: '..startSize..' '..endSize)
	end
end

function LogBreak(parameter, seconds)
	if (DEBUG) then
print('LogBreak '..parameter..' '..seconds..'(')
end
	if dailyTbl[parameter] then
		dailyTbl[parameter] = dailyTbl[parameter]..","..seconds
	else
		dailyTbl[parameter] = seconds
	end

	writeTodaysParameterFile()
	if (DEBUG) then
print(')')
end
end

function CheckBreakTimer()
	if (DEBUG) then
		print('CheckBreakTimer(')
	end
	local idleTime = GetIdleTime()
	local ignore = GetBreakIgnoreInSeconds() - 30
	local meterBreakTimer = SKIN:GetMeter('BreakTime')

	if not breakTimerShown and idleTime >= ignore then
		if (DEBUG) then
			print('show')
		end
		SKIN:Bang('[!SetOption mBreakCountDown Paused "0"]')
		--SKIN:Bang('!SetOption', 'mBreakCountDown', 'Paused', '0')
		meterBreakTimer:Show()
		breakTimerShown = true
	elseif breakTimerShown and idleTime < ignore then
		if (DEBUG) then
			print('hide')
		end
		SKIN:Bang('[!SetOption mBreakCountDown Paused "1"]')
		--SKIN:Bang('!SetOption', 'mBreakCountDown', 'Paused', '1')
		SKIN:Bang('!CommandMeasure mBreakCountDown "Reset"')
		meterBreakTimer:Hide()
		breakTimerShown = false
	elseif DEBUG then
		print('nothing')
	end
	if (DEBUG) then
		print(')')
	end
end

-- Schreibt die aktuelle Zeit in eine Datei
function tick()
	if (DEBUG) then
print('tick(')
end
	local idleTime = GetIdleTime()
	local now = GetNow()
	local ignore = GetBreakIgnoreInSeconds()
	local lastTickDifference = 0
	if (tonumber(getLastTick()[1]) > 0) then
		lastTickDifference = now - getLastTick()[1]
	end

	if (DEBUG) then
print('idle: '..idleTime..'; now: '..now..'; ignore: '..ignore..'; breakActive: '..breakActive..'; lastTick: '..lastTickDifference)
end

	-- idleTime is not recognized if the computer was turned into energy-save-mode. Therefore the tick difference needs always to be taken into account.
	if (lastTickDifference > GetUpdateIntervallInSeconds() * 2) then
		idleTime = math.max(lastTickDifference, idleTime)
	end

	if idleTime < tonumber(breakActive) then
		local breakTime = breakActive + idleTime
		if (DEBUG) then
print('Stoppe pause der Länge: '..breakTime..' '..breakActive..' '..idleTime)
end
		dailyTbl['break'] = dailyTbl['break'] + breakTime

		-- Note down the break ending time
		LogBreakEnd(now)

		breakActive = 0

	elseif idleTime >= ignore then
		if breakActive == 0 then
			if (DEBUG) then
print('Aktiviere Pausenzähler')
end
			-- Note down the break starting time
			LogBreakStart(now - idleTime)
		end

		breakActive = idleTime
	else
		CheckBreakTimer()
	end

	ioHelp.WriteTempFile(GetDailyTickFileName(today), GetNow()..' '..idleTime, 'w')
	if (DEBUG) then
print('tick:'..GetDailyTickFileName(today)..' '..GetNow()..')')
end
	return true
end

function HandleWorkDayEnd(lastdate)
	if (DEBUG) then
		print('HandleWorkDayEnd: ' .. lastdate..'(')
	end
	local oldParameterFile = GetDailyLogFileName(lastdate)
	local oldTbl = ioHelp.ReadParameterFile(oldParameterFile)
	local oldTickFile = GetDailyTickFileName(lastdate)
	local oldTick = help.split(help.getFirstLine(oldTickFile))[1]

	oldTbl['worked'] = oldTick - oldTbl['starttime'] - oldTbl['break']

	-- Delete old tick-file
	os.remove(ioHelp.GetTempFilePath(oldTickFile))

	-- Save work time result for specific date
	ioHelp.WriteParameterFile(oldParameterFile, oldTbl, 'w')

	-- Update ini file with past worked time for current time-to-work calculation.
	ini[lastdate] = {}
	for i,v in pairs(oldTbl) do
		if (DEBUG) then
print('copy: '..i..':'..v)
end
		ini[lastdate][i] = v
	end

	-- Calculate the new time-to-work per day and per week parameter.
	ini['settings']['week'] = ini['settings']['week'] + oldTbl['worked']

	ioHelp.WriteLogHistory()
	if (DEBUG) then
print(')')
end
end

function Update()
	if (DEBUG) then
print('Update(')
end
	if not initialized then
		if (DEBUG) then
print(initialized)
		print(')')
end
		return 0
	end
	tick()
	local result = timePassedTodayPercentage()
	help.SetValue('WeekWorked', timePassedThisWeek())
	if (DEBUG) then
print(')')
end
	return result
end

-- Getter functions ----------------
function timePassedToday()
	if (DEBUG) then
print('timePassedToday(')
end
	local result = GetNow() - dailyTbl['starttime'] - dailyTbl['break'] - breakActive
	if (DEBUG) then
print(result..')')
end
	return tonumber(result)
end

function timePassedTodayPercentage()
	if (DEBUG) then
print('timePassedTodayPercentage(')
end
	local result = timePassedToday() / dailyTbl['secondstowork']
	if (DEBUG) then
print('Percentage:'..result..')')
end
	return tonumber(result)
end

function timePassedThisWeek()
	if (DEBUG) then
print('timePassedThisWeek(')
end
	local result = (ini['settings']['week'] + timePassedToday()) / GetSecondsToWorkPerWeek()
	if (DEBUG) then
print(result..')')
end
	return result
end

function GetSecondsToWorkPerWeek()
	local result = SKIN:GetVariable('week', '40') * 60 * 60
	if (DEBUG) then
print('GetSecondsToWorkPerWeek: '..result)
end
	return result
end

function GetDailyLogFileName(today)
	if (DEBUG) then
print('GetDailyLogFileName: ' .. today)
end
	return today..dailyLogFileEnding
end

function GetDailyTickFileName(today)
	if (DEBUG) then
print('GetDailyTickFileName: ' .. today)
end
	return today..dailyTickFileEnding
end

function GetBreakIgnoreInSeconds()
	local result = SKIN:GetVariable('ignoreBreak', '5') * 60
	if (DEBUG) then
print('GetBreakIgnoreInSeconds: '..result)
end
	return tonumber(result)
end

function GetUpdateIntervallInSeconds()
	local result = SKIN:GetVariable('updateSeconds', '30')
	if (DEBUG) then
print('GetUpdateIntervallInSeconds: '..result)
end
	return tonumber(result)
end

function GetIdleTime()
	local result = SKIN:GetMeasure('mIdleTime'):GetValue()
	if (DEBUG) then
print('GetIdleTime: '..result)
end
	return tonumber(result)
end

function GetNow()
	local time = os.date('*t')
	local now = time.hour
	now = time.min + (now * 60)
	now = time.sec + (now * 60)
	if (DEBUG) then
print('GetNow:'..now)
end
	return now
end

function GetTimeFromSeconds(seconds)
	if (DEBUG) then
print('GetTimeFromSeconds: '..seconds)
end
	local minutes = (seconds / 60) % 60
	local hours = seconds / (60 * 60)
	if (DEBUG) then
		print(hours..' '..minutes..' '..string.format("%02d:%02d", hours,minutes))
	end
	return string.format("%02d:%02d", hours,minutes)
end

--GUI-Setting functions --------------------
function SetWorkTimeDisplay()
	local startT = GetTimeFromSeconds(tonumber(dailyTbl['starttime']))
	local endT = GetTimeFromSeconds(dailyTbl['secondstowork'] + dailyTbl['starttime'] + dailyTbl['break'])
	local result = startT..' - '..endT
	SKIN:Bang('!SetOption', 'StartTime', 'Text', result)
	if (DEBUG) then
		print('SetWorkTimeDisplay: '..result)
	end
end

function SetBreakLogDisplay(display, startSeconds, endSeconds)
	local startT = GetTimeFromSeconds(startSeconds)
	local endT = GetTimeFromSeconds(endSeconds)
	local result = GetTimeFromSeconds(startSeconds)..' - '..GetTimeFromSeconds(endSeconds).." : "..GetTimeFromSeconds(endSeconds - startSeconds)
	SKIN:Bang('!SetOption', 'BreakLog'..display, 'Text', result)
	SKIN:Bang('!SetOption', 'BreakLog'..display..'Del', 'Hidden', '0')
	print('BreakLog'..display..": "..result)
end

function ResetBreakLogDisplay(display)
	SKIN:Bang('!SetOption', 'BreakLog'..display, 'Text', '')
	SKIN:Bang('!SetOption', 'BreakLog'..display..'Del', 'Hidden', '1')
end
