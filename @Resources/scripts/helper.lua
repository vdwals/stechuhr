help = {}

function help.SetValue(Meter,Value)
	if (DEBUG) then
			print('help.SetValue: '..Meter..', '..Value)
		end
   local mHandle=assert(SKIN:GetMeter(Meter),'Invalid Meter '..Meter)
   local MeasureName=assert(mHandle:GetOption('MeasureName'),'MeasureName not found in '..Meter)
   if assert(SKIN:GetMeasure(MeasureName),'Invalid Measure '..MeasureName) then
      SKIN:Bang('!SetOption',MeasureName,'Formula',Value)
      SKIN:Bang('!UpdateMeasure',MeasureName)
   end
end

-- Returns day of week for a given date string dd-mm-yyyy
function help.GetDayOFWeek(date_str)
	if (DEBUG) then
			print('help.GetDayOFWeek: '..date_str..' (')
		end
	local day, month, year
	if not weekDays then
		if (DEBUG) then
			print('Initialize weekDays')
		end
		weekDays = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" }
	end

	x = string.gsub(date_str, "(%d+)-(%d+)-(%d+)", function (mm,dd,yyyy)
			day = tonumber(dd)
			month = tonumber(mm)
			year = tonumber(yyyy)
		end)

	local mmx = month
	if (month == 1) then
		mmx = 13
		year = year-1
	end
  if (month == 2) then
		mmx = 14
		year = year-1
	end

  local val8 = day + (mmx*2) +  math.floor(((mmx+1)*3)/5)   + year + math.floor(year/4)  - math.floor(year/100)  + math.floor(year/400) + 2
  local val9 = math.floor(val8/7)
  local dw = val8-(val9*7)

  if (dw == 0) then
    dw = 7
  end

	if (DEBUG) then
			print(dw..':'..weekDays[dw]..')')
		end
  return dw
end

function help.GetWorkdayOfWeek(date_str, weekStartDay)
	if (DEBUG) then
			print('help.GetWorkdayOfWeek: '..date_str..', '..weekStartDay..'(')
		end
	local dayOfWeek = help.GetDayOFWeek(date_str)
	if (DEBUG) then
			print(dayOfWeek)
		end
	local startOfWeekIndex, workdayOfWeek
	for i,v in ipairs(weekDays) do
		if (DEBUG) then
			print(i..':'..v)
		end
		if v == weekStartDay then
			startOfWeekIndex = i
			break
		end
	end

	if (DEBUG) then
			print(startOfWeekIndex)
		end

	if startOfWeekIndex > dayOfWeek then
		workdayOfWeek = 7 - (startOfWeekIndex - dayOfWeek)
	else
		workdayOfWeek = dayOfWeek - startOfWeekIndex
	end

	if (DEBUG) then
			print(workdayOfWeek..')')
		end
	return workdayOfWeek
end

-- Split-String function
function help.split(inputstr, sep)
	local t={} ; i=1
	if (inputstr == nil) then
		return t
	end
	if (DEBUG) then
			print('split: '..inputstr..'(')
		end
	--local sep = ";"
  if sep == nil then
    sep = "%s"
  end
  for str in string.gmatch(inputstr, '([^'..sep..']+)') do
    t[i] = str
		if (DEBUG) then
			print(i..': '..str)
		end
    i = i + 1
  end
	if (DEBUG) then
			print(')')
		end
  return t
end

-- Reads the given file and returns the first line.
function help.getFirstLine(file)
	if (DEBUG) then
			print('getFirstLine: '..file..'(')
		end
	local lines = ioHelp.ReadTempFile(file)
	if help.count(lines) >= 1 and lines[1] ~= nil	then
		if (DEBUG) then
			print('Read ' .. lines[1] .. ' from ' .. file..')')
		end
		return lines[1]
	end
	if (DEBUG) then
			print('Read nothing from ' .. file..')')
		end
	return 0
end

function help.count(array)
	if (DEBUG) then
			print('count(')
		end
	if array == nil then
		return 0
	end

	local counter = 0
	--for element in array do
	for k, v in pairs(array) do
		counter = counter + 1
	end
	if (DEBUG) then
			print('count_result: '..counter.." - "..#array..')')
		end
	return counter
end
