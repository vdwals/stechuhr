@echo off
:: %1 command: start, restart
:: %2 Project
:: [%3 Flow|Options]

SET DIR=%~dp0

cd C:\X-Gen\4.6-r60412
if "%XGEN_ROOT%" == "" (
  call "%~dp0env.bat" >nul 2>&1
)
:: statistics. Example Call: admin.bat statistics Calc [Calculation]
C:\X-Gen\4.6-r60412\bin\Windows_x64\xgenadmin.exe -auth admin:admin %1  %2 %3 -trace-mode Active -clear-trace > "%DIR%result.txt"

cd %DIR%
