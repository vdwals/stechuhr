[Variables]
;-- System settings
Language=German
Location=GMXX0040
Unit=m
ProcessInterval=1

;-- Settings for working time clock
day=8
week=39.5
;week=31.6
holidaysPerMonth=2.5

; When does your week start? possible values: Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday; Default: Monday
weekStartDay=Monday
; What days of the week do you not work? possible values: Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday; Default: Saturday, Sunday
noWorkDay=Saturday, Sunday

; Time measure settings for precision
updateSeconds=30
; Minutes of idle time not listed as break
ignoreBreak=5

; Start angle of work timer circle
startangle=4.7123889803846898576939650749193
; Full angle of work timer circle
rotationangle=6.283185307179586476925286766559
; Size
size=350

;-- Server GUI settings
user=
password=
url=
port=

;-- General settings
;TextColor1=0,116,1
;TextColor2=255,255,255
;PrimaryColor=114, 186, 116
;SecondaryColor=61, 108, 16
;BothColor=69, 171, 72

; Color settings
MainColor=255,255,255
Alpha1=255
Alpha2=150
Alpha3=80

hoursworkedcolor=99,130,55
overhoursworkedcolor=180,52,52
hourstoworkcolor=#MainColor#
weektoworkcolor=#MainColor#
weekworkedcolor=99,130,55

TextColor1=#MainColor#,#Alpha1#
TextColor2=#MainColor#,#Alpha2#
PrimaryColor=#MainColor#,#Alpha1#
SecondaryColor=#MainColor#,#Alpha2#
BothColor=#MainColor#,#Alpha3#
LineColor=#MainColor#,#Alpha3#

FontFace=Helvetica 25 UltraLight
